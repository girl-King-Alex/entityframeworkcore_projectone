﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace EFCoreDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var book = new Textbook() { TextbookName = "vitality is key", StudentId = 1 };
            var book2 = new Textbook() { TextbookName = "vitality is key", StudentId = 2 };
            var student = new Student() { studentName = " macaroni" };
            //AddToDatabase<Student>(student);
            // AddToDatabase<Textbook>(book);
            //AddToDatabase<Textbook>(book2);


            /*var result = GetItemFromDatabase<Textbook>(2);
            Console.WriteLine(result?.TextbookName);*/

            //DeleteItemFromDatabase<Lecturer>(5);
            ProcessingExpelledStudents(1);
        }

        static int AddToDatabase<Tentity>(Tentity entity) where Tentity : class
        {
            int AffectedRow;
            try
            {
                using (var context = new Dbcontext())
                {
                    Console.WriteLine("started");
                    context.Add(entity);
                    AffectedRow = context.SaveChanges();
                    Console.WriteLine("Added");
                    return AffectedRow;
                }
            }
            catch (DbUpdateException dbupdateError)
            {
                Console.WriteLine("\nError!!!!");
                Console.WriteLine("Error Source: {0}", dbupdateError.Source);
                Console.WriteLine("The Id is an auto incremented primary key, Do not try to populate it!");
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                Console.WriteLine("Error Source: {0}", error.Source);
            }
            return 0;
        }

        static int UpdateStudentInfo(Student student, int id)
        {
            int affectedRow;
            try
            {
                using (var context = new Dbcontext())
                {
                    var retrievedAccount = context.Students.Find(id);
                    if (retrievedAccount != null)
                    {
                        Console.WriteLine("Starting Update");
                        retrievedAccount.studentName = student.studentName ??= retrievedAccount.studentName;
                        retrievedAccount.studentId = student.studentId ??= retrievedAccount.studentId;
                        Console.WriteLine("Updated Account");
                        affectedRow = context.SaveChanges();
                        return affectedRow;
                    }
                    else
                    {
                        Console.WriteLine("User not found!");
                    }
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
            }
            return 0;
        }

        static Tentity GetItemFromDatabase<Tentity>(int Id) where Tentity : class
        {
            Tentity result = null;
            try
            {
                using (var context = new Dbcontext())
                {
                    result = (Tentity)context.Find(typeof(Tentity), Id);
                    if (result != null)
                    {
                        return result;
                    }
                    throw new Exception($"Could not find row with id: {Id} in Table");
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
            }
            return null;
        }

        static int DeleteItemFromDatabase<Tentity>(int Id) where Tentity : class
        {
            int affectedRow;
            try
            {
                using (var context = new Dbcontext())
                {
                    Tentity result = (Tentity)context.Find(typeof(Tentity), Id);
                    if (result != null)
                    {
                        context.Remove(result);
                        affectedRow = context.SaveChanges();
                        Console.WriteLine("Delete Successful ");
                        return affectedRow;
                    }
                    Console.WriteLine($"Could not find row with id: {Id} in Table");
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
            }
            return 0;
        }

        static void ProcessingExpelledStudents(int Id)
        {
            IDbContextTransaction transaction = null;
            try
            {
                using (var context = new Dbcontext())
                {
                    using (transaction = context.Database.BeginTransaction())
                    {
                        //get student and all her books
                        var student = context.Students.Find(Id);
                        var books = context.Textbooks.Where(x => x.StudentId == Id);

                        //remove student
                        if (student != null)
                        {
                            context.Students?.Remove(student);
                            if(books.Count() > 0)
                            {
                                foreach (var item in books)
                                    context.Textbooks?.Remove(item);                                
                            }
                            context.SaveChanges();
                            Console.WriteLine($"Deleted Student with  Id : {Id}");
                            Console.WriteLine($"Deleted books belonging to Student with  Id : {Id}");
                        }
                        else
                        {
                            Console.WriteLine("Student not found");
                            return;
                        }

                        //remove her books if any
                        

                        //add student to black book
                        var exStudent = new ExpelledStudent { StudentId = student.studentId, studentName = student.studentName };
                        context.ExpelledStudents.Add(exStudent);
                        context.SaveChanges();
                        Console.WriteLine($"Added Student with  Id : {Id} to black book");

                        transaction.Commit();
                    }
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                transaction.Rollback();
            }

        }
    }
}
