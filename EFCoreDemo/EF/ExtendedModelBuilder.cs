﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDemo
{
    public static class ExtendedModelBuilder
    {
        public static void seed(this ModelBuilder modelBuilder)
        {
            
            var students = new List<Student>
            {
               new Student { studentId = 1, studentName = "Alexa" ,},
               new Student { studentId = 2, studentName = "KamKam"},
               new Student { studentId = 3, studentName = "KOKO" },
               new Student { studentId = 4, studentName = "Kamsy"},
               new Student { studentId = 5, studentName = "Kaima" },
            };

            var courses = new List<Course>
            {
                new Course {courseId= 1, courseName="Bio 419"},
                new Course {courseId= 2, courseName="Bio 856"},
                new Course {courseId= 3, courseName="Bio 101"},
                new Course {courseId= 4, courseName="Bio 351"},
                new Course {courseId= 5, courseName="Bio 352"},
            };

            var lecturers = new List<Lecturer>
            {
                new Lecturer { LecturerId=1, CourseId=1, LecturerName= "akanwu"},
                new Lecturer { LecturerId=2, CourseId=2, LecturerName= "bolatina"},
                new Lecturer { LecturerId=3, CourseId=3, LecturerName= "creatindogwy"},
                new Lecturer { LecturerId=4, CourseId=4, LecturerName= "Drllo tee"  },
                new Lecturer { LecturerId=5, CourseId=5, LecturerName= "Rinlaty"  }
            };

            var books = new List<Textbook>
            {
                new Textbook { TextbookId = 1 , TextbookName ="Essentials of bio" },
                new Textbook { TextbookId = 2,  TextbookName ="Essentials of bio 856"  },
                new Textbook { TextbookId = 3,  TextbookName ="Essentials of bio 101"  },
                new Textbook { TextbookId = 4,  TextbookName ="Essentials of bio 351" },
                new Textbook { TextbookId = 5 , TextbookName ="Essentials of bio 352"},
            };            

            for 
                (var i = 0; i < courses.Count; i++)
            {              
                
                books[i].CourseId = courses[i].courseId;
                books[i].StudentId = students[i].studentId;
            }

            modelBuilder.Entity<Student>().HasData(students);
            modelBuilder.Entity<Course>().HasData(courses);
            modelBuilder.Entity<Lecturer>().HasData(lecturers);
            modelBuilder.Entity<Textbook>().HasData(books);

           
        } 
    }
}
