﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDemo
{
    class Dbcontext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Lecturer> Lecturers { get; set; }
        public DbSet<Textbook> Textbooks { get; set; }
        public DbSet<ExpelledStudent> ExpelledStudents { get; set; }

        protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder)
        {           
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB; Database=SchoolDB;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.seed();
        }
    }
}
