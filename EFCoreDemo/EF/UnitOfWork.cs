﻿using System;
using System.Collections.Generic;
using System.Text;
using EFCoreDemo;

namespace QuestionTwo.EF
{
    class UnitOfWork
    {
        Student _student;
        Textbook _book;
        Dbcontext _context;

        public UnitOfWork (Dbcontext context)
        {
            _context = context;
        }

        public Student student
        {
            get
            {
                if (_student == null)
                {
                    _student = new Student(_context);
                }
                return _student;
            }
        }

        public Student book
        {
            get
            {
                if (_book == null)
                {
                    _book = new Textbook(_context);
                }
                return _student;
            }
        }

        public void Save ()
        {
            _context.SaveChanges();
        }
    }
}
