﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EFCoreDemo
{
    class Course
    {
        [Key]
        public int? courseId { get; set; }
        [MaxLength(50)]
        public string courseName { get; set; }
        
    }
}
