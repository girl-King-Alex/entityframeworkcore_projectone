﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EFCoreDemo
{
    class Lecturer
    {
        [Key]
        public int? LecturerId { get; set; }
        [MaxLength(50)]
        public string LecturerName { get; set; }        
        public int? CourseId { get; set; }
        [ForeignKey(nameof(CourseId))]        
        public Course Course { get; set; } 
       
        
    }
}
