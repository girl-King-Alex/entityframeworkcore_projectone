﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EFCoreDemo
{
    class Student
    {
        Dbcontext _context;
        public Student() { }
        public Student(Dbcontext dbcontext)
        {
            _context = dbcontext; 
        }
        [Key]
        public int? studentId { get; set; }
        [MaxLength(50)]
        public string studentName { get; set; }               
        
        public List<Textbook> Textbooks { get; set; } = new List<Textbook>();
    }
}
