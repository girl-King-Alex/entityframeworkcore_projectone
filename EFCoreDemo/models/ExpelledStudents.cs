﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EFCoreDemo
{
    class ExpelledStudent
    {
        [Key]
        public int? ExpelledStudentId { get; set; }
        [MaxLength(50)]
        public string studentName { get; set; }  
        
        public int? StudentId { get; set; }
    }
}
