﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EFCoreDemo
{
    class Textbook
    {
        Dbcontext _context;
        public Textbook() { }
        public Textbook(Dbcontext dbcontext)
        {
            _context = dbcontext;
        }
        [Key]
        public int? TextbookId { get; set; }
        public String TextbookName { get; set; }
        public int? StudentId { get; set; }
        public int? CourseId { get; set; }
        [ForeignKey(nameof(StudentId))]
        public Student BookOwner { get; set; }
        [ForeignKey(nameof(CourseId))]
        public Course Relatedcourse { get; set; }
    }
}
