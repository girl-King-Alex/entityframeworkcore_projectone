﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCoreDemo.Migrations
{
    public partial class CreateDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    courseId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    courseName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.courseId);
                });

            migrationBuilder.CreateTable(
                name: "ExpelledStudents",
                columns: table => new
                {
                    ExpelledStudentId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    studentName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    StudentId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExpelledStudents", x => x.ExpelledStudentId);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    studentId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    studentName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.studentId);
                });

            migrationBuilder.CreateTable(
                name: "Lecturers",
                columns: table => new
                {
                    LecturerId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LecturerName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CourseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lecturers", x => x.LecturerId);
                    table.ForeignKey(
                        name: "FK_Lecturers_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "courseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Textbooks",
                columns: table => new
                {
                    TextbookId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TextbookName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StudentId = table.Column<int>(type: "int", nullable: true),
                    CourseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Textbooks", x => x.TextbookId);
                    table.ForeignKey(
                        name: "FK_Textbooks_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "courseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Textbooks_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "studentId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "courseId", "courseName" },
                values: new object[,]
                {
                    { 1, "Bio 419" },
                    { 2, "Bio 856" },
                    { 3, "Bio 101" },
                    { 4, "Bio 351" },
                    { 5, "Bio 352" }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "studentId", "studentName" },
                values: new object[,]
                {
                    { 1, "Alexa" },
                    { 2, "KamKam" },
                    { 3, "KOKO" },
                    { 4, "Kamsy" },
                    { 5, "Kaima" }
                });

            migrationBuilder.InsertData(
                table: "Lecturers",
                columns: new[] { "LecturerId", "CourseId", "LecturerName" },
                values: new object[,]
                {
                    { 1, 1, "akanwu" },
                    { 2, 2, "bolatina" },
                    { 3, 3, "creatindogwy" },
                    { 4, 4, "Drllo tee" },
                    { 5, 5, "Rinlaty" }
                });

            migrationBuilder.InsertData(
                table: "Textbooks",
                columns: new[] { "TextbookId", "CourseId", "StudentId", "TextbookName" },
                values: new object[,]
                {
                    { 1, 1, 1, "Essentials of bio" },
                    { 2, 2, 2, "Essentials of bio 856" },
                    { 3, 3, 3, "Essentials of bio 101" },
                    { 4, 4, 4, "Essentials of bio 351" },
                    { 5, 5, 5, "Essentials of bio 352" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Lecturers_CourseId",
                table: "Lecturers",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Textbooks_CourseId",
                table: "Textbooks",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Textbooks_StudentId",
                table: "Textbooks",
                column: "StudentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExpelledStudents");

            migrationBuilder.DropTable(
                name: "Lecturers");

            migrationBuilder.DropTable(
                name: "Textbooks");

            migrationBuilder.DropTable(
                name: "Courses");

            migrationBuilder.DropTable(
                name: "Students");
        }
    }
}
